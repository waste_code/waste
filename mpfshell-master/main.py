import machine
from machine import Pin
import socket
import utime
import network
import sys

def net_init():
	#print('inizio net_init')
	sta_if = network.WLAN(network.STA_IF)
	if sta_if.isconnected():
		return sta_if

	sta_if.active(True)
	#sta_if.ifconfig(('192.168.1.253', '255.255.255.0', '192.168.1.1', '8.8.8.8'))
	sta_if.connect('lillis_home', 'LaPasswordNonTeLaDic0!')
	utime.sleep(5)
	#print ('Stato connessione: {}'.format(sta_if.isconnected()))
	return sta_if

	# configure RTC.ALARM0 to be able to wake the device
def rtc_init():
	#print('inizio rtc_init')
	rtc = machine.RTC()
	rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)	
	return rtc

def pin_init():
	#print('inizio pin_init')
	p2 = Pin(2, Pin.OUT) #p2 led enbedded sulla scheda
	p2.high()
	p1 = Pin(13, Pin.IN)#switch che esegue il main o si mette nel prompt'''
	p3 = Pin(5, Pin.IN)#legge stato contatto
	return p1,p2,p3

def goto_sleep():
	#print('inizio goto_sleep')
	# set RTC.ALARM0 to fire after 5 seconds (waking the device)
	rtc.alarm(rtc.ALARM0, 5000)

	# put the device to sleep
	machine.deepsleep()
	return 

p1, p2, p3=pin_init()
timeout=0
if p1.value() == 0 :

	sta_if = net_init()
	rtc = rtc_init()

	addr = ['192.168.1.5', 10000]

	print('woke from a deep sleep - Stato connessione: {}'.format(sta_if.isconnected()))
	#p2.low()
	
	index = 0
	while not sta_if.isconnected() and index < 5:
		print('RETRY: {})my addr: {} - active: {} - connected: {}'.format(index,sta_if.ifconfig()[0], sta_if.active(), sta_if.isconnected()))
		utime.sleep(3)
		index=index+1
	
	if index<5:
		p2.high()
		#s.connect(addr)
		print('SEND: remote addr: {}:{} - active: {} - connected: {}'.format(addr[0],addr[1], sta_if.active(), sta_if.isconnected()))
		print (addr)
		if timeout == 0:
			stato = '0 - {}'.format(p3.value())
		else:
			stato = '1 - {}'.format(p3.value())

		s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		print('invio stato: {}'.format(s.sendto(stato, addr)))
		utime.sleep(1)
		s.settimeout(5)
		
		try:
			print (s.recv(3))
			timeout=0
		except:
			timeout=1
		
		p2.low()
	
	goto_sleep()

else:
	print('prompt condition')