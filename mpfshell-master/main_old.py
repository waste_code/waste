import machine
from machine import Pin
import socket
import utime
import network

print('Eseguo il main')

# configure RTC.ALARM0 to be able to wake the device

sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
sta_if.ifconfig(('192.168.1.254', '255.255.255.0', '192.168.1.1', '8.8.8.8'))

sta_if.connect('lillis_home', 'LaPasswordNonTeLaDic0!')
sta_if.isconnected()

rtc = machine.RTC()
rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
p2 = Pin(2, Pin.OUT) #p2 led enbedded sulla scheda
p2.high()
p1 = Pin(13, Pin.IN)#switch che esegue il main o si mette nel prompt

addr = ['192.168.1.2', 10000]
s=socket.socket()

if p1.value() == 0 or 1==1:
	# check if the device woke from a deep sleep
	if 1==1 or machine.reset_cause() == machine.DEEPSLEEP_RESET:
	    print('woke from a deep sleep')
	    p2.low()
	    
	    
	    while not sta_if.isconnected():
	    	print('try connection')
	    	utime.sleep(1)
	    	print(sta_if.ifconfig())
	    	print(sta_if.active())
	    	print(sta_if.isconnected())

	    
	    s.connect(addr)
	    stato = 'tutto ok - {}\n'.format(p1.value())
	    s.send(stato)

	# set RTC.ALARM0 to fire after 1 second (waking the device)
	rtc.alarm(rtc.ALARM0, 1000)

	# put the device to sleep
	machine.deepsleep()
else:
	print('prompt condition')
