import machine
from machine import Pin
import socket
import utime
import network
import sys

def net_init():
	print('inizio net_init')
	sta_if = network.WLAN(network.STA_IF)
	if sta_if.isconnected():
		return sta_if

	sta_if.active(True)
	sta_if.ifconfig(('192.168.1.253', '255.255.255.0', '192.168.1.1', '8.8.8.8'))
	sta_if.connect('lillis_home', 'LaPasswordNonTeLaDic0!')
	utime.sleep(3)
	return sta_if

	# configure RTC.ALARM0 to be able to wake the device
def rtc_init():
	print('inizio rtc_init')
	rtc = machine.RTC()
	rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)	
	return rtc

def pin_init():
	print('inizio pin_init')
	p2 = Pin(2, Pin.OUT) #p2 led enbedded sulla scheda
	p2.high()
	p1 = Pin(13, Pin.IN)#switch che esegue il main o si mette nel prompt'''
	return [p1,p2]

def goto_sleep():
	print('inizio goto_sleep')
	# set RTC.ALARM0 to fire after 1 second (waking the device)
	rtc.alarm(rtc.ALARM0, 1000)

	# put the device to sleep
	machine.deepsleep()
	return 

print('Eseguo il main')
sta_if = net_init()
rtc = rtc_init()
pin_set=pin_init()
p1=pin_set[0]
p2=pin_set[1]

addr = ['192.168.1.2', 10000]
s=socket.socket()

if p1.value() == 0 :
	# check if the device woke from a deep sleep
	if machine.reset_cause() == machine.DEEPSLEEP_RESET:
		print('woke from a deep sleep')
		p2.low()
		
		index = 0
		while not s.isconnected() and index < 5:
			print('try connection')
			utime.sleep(3)
			print(sta_if.ifconfig())
			print(sta_if.active())
			print(sta_if.isconnected())
			sta_if.active(True)
			sta_if.connect('lillis_home', 'LaPasswordNonTeLaDic0!')
			index=index+1
		
		if sta_if.isconnected():
			s.connect(addr)
			stato = 'tutto ok - {}\n'.format(p1.value())
			s.send(stato)

	goto_sleep()

else:
	print('prompt condition')