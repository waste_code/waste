import machine
from machine import Pin
import socket
import network
from utime import sleep_ms

def pin_init():
    p1 = Pin(13, Pin.IN)#switch che esegue il main o si mette nel prompt''' - corrisponde a GPIO13 su schedina
    p2 = Pin(4, Pin.IN) #switch che fa partire come AP o HOST''' - corrisponde a GPIO04 su schedina
    
    return p1,p2

def ap_start():
    print("inibisco host")
    network.WLAN(network.STA_IF).active(False)  #disattiva modalità host
    print("setto indirizzo")
    ap = network.WLAN(network.AP_IF)
    #ap.ifconfig(('10.0.0.1', '255.255.255.0', '10.0.0.1', '8.8.8.8')) #imposto indirizzo AP
    ap.ifconfig(('192.168.0.1', '255.255.255.0', '192.168.0.1', '192.168.0.1'))
    ap.config(essid='shutter', channel=11, password='password')
    # Query params one by one
    #print(ap.config('essid'))
    #print(ap.config('channel'))

    ap.active(True)    #attiva modalità access point
    
def build_page():
    pins = [machine.Pin(i, machine.Pin.IN) for i in (0, 2, 4, 5, 12, 13, 14, 15)]

    html = """<!DOCTYPE html>
    <html>
    <head></head>
    <body> 
    <div>
    <h1> Messa a Fuoco </h1><form action="Fuoco" method="SESSION"><button type="submit">Fuoco</button></form>
    </div>
    <br>
    <div>
    <h1> Scatto </h1><form action="Scatta" method="SESSION"><button type="submit">Scatta</button></form>
    </div>
    <div>
    <h1> Messa a Fuoco e Scatto</h1><form action="FuocoScatta" method="SESSION"><button type="submit">Fuoco&Scatta</button></form>
    </div>
    <div>
    <h1> Time Lapse</h1>
    <form action="/timelapse" method="get" target="_blank">
    Numero di scatti: <input type="text" name="num_s"><br>
    Intervallo di scatto: <input type="text" name="int_s"><br>
    <button type="submit" value="Submit">TimeLapse</button>
    </form>
    </div>
    </body>
    </html>\n\r
    """ #<table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table> || <button type="submit" value="Submit">TimeLapse</button>

   
    #rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in pins]
    #response = html % '\n'.join("<meta http-equiv=\"refresh\" content=\"1;URL=http://192.168.0.1/>")
    
    response = html
    return response  

def fuoco():
    print ("trovato -> Fuoco <-")
    return
def scatta():
    print ("trovato -> Scatta <-")
    return
def fuocoEscatta():
    print ("trovato -> Fuoco&Scatta <-")
    return

runner_pin,mode = pin_init()

if runner_pin.value() == 0: #programma o prompt

    if mode.value() == 0:   #AP o host
        ap_start()
    else:
        ap_start()

    print(network.WLAN(network.AP_IF).ifconfig())

    #addr  = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('',80))
    s.listen(1)

    print('listening...')

    while True:     
        cl, addr = s.accept()
        cl_file = cl.makefile('rwb', 0)
        while True:
            line = cl_file.readline()
            if not line or line == b'\r\n':
                break
            #print(line)
            if 'GET /Fuoco?' in line and not 'Scatta' in line:
                fuoco()
            if 'GET /Scatta' in line and not 'Fuoco' in line:          
                scatta()
            if 'GET /FuocoScatta?' in line:
                fuocoEscatta()

        r=build_page()
        cl.send(r)
        #print('client connected from', addr)
        cl.close()
        #print(network.WLAN(network.AP_IF).ifconfig())
else:
    print('prompt condition')