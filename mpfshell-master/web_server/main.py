import machine
from machine import Pin
import socket
import network
from utime import sleep_ms

#PIN MAP
#
#   GPIO04 - OUT -> FUOCO
#   GPIO05 - OUT -> SCATTO
#   GPIO15 - shall be low
#   GPIO02 - not used
#   GPIO00 - shall be high or floating
#   GPIO14 - not used
#   GPIO12 - not used
#   GPIO13 - HIGH: prompt - LOW: main.py
#


def trigger_scatto():
    return

def trigger_focus():
    return

def pin_init():
    p1 = Pin(13, Pin.IN)#switch che esegue il main o si mette nel prompt''' - corrisponde a GPIO13 su schedina
    p2 = Pin(4, Pin.OUT)#pin di comando dello scatto
    p3 = Pin(5, Pin.OUT)#pin di comando del fuoco
    return p1,p2,p3

def ap_start():
    print("inibisco host")
    network.WLAN(network.STA_IF).active(False)  #disattiva modalità host
    print("setto indirizzo")
    ap = network.WLAN(network.AP_IF)
    #ap.ifconfig(('10.0.0.1', '255.255.255.0', '10.0.0.1', '8.8.8.8')) #imposto indirizzo AP
    ap.ifconfig(('192.168.0.1', '255.255.255.0', '192.168.0.1', '192.168.0.1'))
    ap.config(essid='shutter', channel=11, password='password')
    # Query params one by one
    #print(ap.config('essid'))
    #print(ap.config('channel'))

    ap.active(True)    #attiva modalità access point
    
def build_page():
    #pins = [machine.Pin(i, machine.Pin.IN) for i in (0, 2, 4, 5, 12, 13, 14, 15)]

    html = """<!DOCTYPE html>
<html>
<head>
<style>
input[type=number]{width:80px;} 
body{background-color: #31415e;}
.button {
width: 300px;
background-color: #555555;
border: 2px solid #008CBA;
border-radius: 12px;
color: white;
text-align: center;
text-decoration: none;
display: inline-block;
font-size: 40px;
margin: 4px 2px;
cursor: pointer;
}
</style> 
</head>
<body> 
<div>
<form action="Fuoco" method="SESSION"><button class="button" type="submit">Fuoco</button></form><br><br>
<form action="Scatta" method="SESSION"><button class="button" type="submit">Scatta</button></form><br><br>
<form action="FuocoScatta" method="SESSION"><button class="button" type="submit">Fuoco&Scatta</button></form><br><br>
<form action="/timelapse" method="SESSION">
<table><tr><td>
<button class="button" type="submit" value="Submit">TimeLapse</button></td>
<td>[#]  <input type="number" min="1" size="7" name="num_s"><br>
[kms]<input type="number" min="0" size="5" name="int_s"><br></td></tr></table>
</form>
</body>
</html>
    """ #<table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table> || <button type="submit" value="Submit">TimeLapse</button>

   
    #rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in pins]
    #response = html % '\n'.join("<meta http-equiv=\"refresh\" content=\"1;URL=http://192.168.0.1/>")
    
    response = html
    return response  

def fuoco(pin):
    print ("trovato -> Fuoco <-")
    pin.value(1)
    print(pin.value())
    sleep_ms(1000)
    print(pin.value())
    pin.value(0)
    print ("trovato -> Fuoco Concluso <-")
    return
def scatto(pin):
    print ("trovato -> Scatta <-")
    pin.value(1)
    print(pin.value())
    sleep_ms(1000)
    print(pin.value())
    pin.value(0)
    print ("trovato -> Scatto Concluso<-")
    return
def fuocoEscatto(fuoco, scatto):
    print ("trovato -> Fuoco&Scatta <-")
    fuoco.value(1)
    sleep_ms(200)
    fuoco.value(0)
    scatto.value(1)
    sleep_ms(200)
    scatto.value(0)
    return

runner_pin, focus_pin, shutter_pin = pin_init()

if runner_pin.value() == 0: #programma o prompt

    ap_start()

    print(network.WLAN(network.AP_IF).ifconfig())

    #addr  = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('',80))
    s.listen(1)

    print('listening...')

    while True:     
        cl, addr = s.accept()
        cl_file = cl.makefile('rwb', 0)
        while True:
            line = cl_file.readline()
            if not line or line == b'\r\n':
                break
            #print(line)
            if 'GET /Fuoco?' in line and not 'Scatta' in line:
                fuoco(focus_pin)
            if 'GET /Scatta' in line and not 'Fuoco' in line:          
                scatto(shutter_pin)
            if 'GET /FuocoScatta?' in line:
                fuocoEscatto(focus_pin, shutter_pin)

        r=build_page()
        cl.send(r)
        #print('client connected from', addr)
        cl.close()
        #print(network.WLAN(network.AP_IF).ifconfig())
else:
    print('prompt condition')