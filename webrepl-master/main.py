import machine
from machine import Pin
import socket
import network

print('Eseguo il main')

# configure RTC.ALARM0 to be able to wake the device

sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
sta_if.ifconfig(('192.168.1.254', '255.255.255.0', '192.168.1.1', '8.8.8.8'))

sta_if.connect('lillis_home', 'LaPasswordNonTeLaDic0')

rtc = machine.RTC()
rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
p2 = Pin(2, Pin.OUT) #p2 led enbedded sulla scheda
p2.high()
p1 = Pin(13, Pin.IN)#switch che esegue il main o si mette nel prompt

if p1.value() == 0:
	# check if the device woke from a deep sleep
	if machine.reset_cause() == machine.DEEPSLEEP_RESET:
	    print('woke from a deep sleep')
	    p2.low()
	    addr = ['192.168.1.2', 10000]
	    
	    while not sta_if.isconnected():
	    	print('try connection')
	    s=socket.socket()
	    s.connect(addr)
	    stato = 'tutto ok - {}\n'.format(p1.value())
	    s.send(stato)

	# set RTC.ALARM0 to fire after 1 second (waking the device)
	rtc.alarm(rtc.ALARM0, 1000)

	# put the device to sleep
	machine.deepsleep()
else:
	print('prompt condition')
